def parse_input(j):
    return [[(record["name"], val) for key, val in record.items() if "Val" in key] for record in j]

