class MagicList(list):
    def __init__(self, cls_type=None):
        self.cls_type = cls_type
        super(MagicList, self).__init__()

    def __setitem__(self, k, v):
        try:
            super(MagicList, self).__setitem__(k, v)
        except IndexError:
            if type(k) is not int:
                raise ValueError("List Index must be a string")
            if k != super(MagicList, self).__len__():
                raise IndexError("list index out of range")
            super(MagicList, self).append(v)

    def __getitem__(self, item):
        try:
            return super(MagicList, self).__getitem__(item)
        except IndexError:
            super(MagicList, self).append(self.cls_type())
            return super(MagicList, self).__getitem__(item)
