from setuptools import setup, find_packages

setup(name="cy-home-test",
      packages=find_packages(),
      install_requires=[
            "sanic",
            "sanic_jwt"
])

