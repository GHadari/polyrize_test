from dataclasses import dataclass
from unittest import main, TestCase
from src.api.json_parser import parse_input
import json

class MainTest(TestCase):
    def test_input(self):
        j = json.loads("""
            [{"name": "device",
             "strVal": "iphone",
             "metadata": "blabla"      
            },
            {
            "name": "isAuth",
            "boolVal": "false",
            "lastSeen": "bah"       
            }]""")
        result = parse_input(j)
        print(result)
        self.assertListEqual([[('device', 'iphone')], [('isAuth', 'false')]], result)

