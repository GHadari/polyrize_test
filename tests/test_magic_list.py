from dataclasses import dataclass
from unittest import main, TestCase
from src.magic_list.magic_list import MagicList


class MainTest(TestCase):
    def test_add_by_index(self):
        a = MagicList()
        a[0] = 5
        self.assertEqual("[5]", str(a))

    def test_data_class(self):
        @dataclass
        class Person:
            age: int = 1

        b = MagicList(cls_type=Person)
        b[0].age = 5
        self.assertEqual("[MainTest.test_data_class.<locals>.Person(age=5)]",
                         str(b))


